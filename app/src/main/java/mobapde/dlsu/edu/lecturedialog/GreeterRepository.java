package mobapde.dlsu.edu.lecturedialog;

/**
 * Created by migscabral on 28/09/2017.
 */

public class GreeterRepository {
    private String mGreetee;

    public GreeterRepository() {
        mGreetee = "World";
    }

    public String getGreetee() {
        return mGreetee;
    }

    public void setGreetee(String greetee) {
        mGreetee = greetee;
    }
}
