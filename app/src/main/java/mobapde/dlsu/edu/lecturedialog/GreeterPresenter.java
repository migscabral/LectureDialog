package mobapde.dlsu.edu.lecturedialog;

import android.view.View;
import android.widget.TextView;

/**
 * Created by migscabral on 28/09/2017.
 */

public class GreeterPresenter {

    private TextView mView;
    private GreeterRepository mGreeterRepository;

    public GreeterPresenter(TextView view, GreeterRepository greeterRepository) {
        mView = view;
        mGreeterRepository = greeterRepository;
    }

    public void updateView() {
        mView.setText("Hello " + mGreeterRepository.getGreetee() + "!");
    }
}
