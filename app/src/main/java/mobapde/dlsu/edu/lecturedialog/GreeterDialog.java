package mobapde.dlsu.edu.lecturedialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * Created by migscabral on 28/09/2017.
 */

public class GreeterDialog extends DialogFragment {

    private GreeterDialogListener mOnClickPositiveListener;

    public void setOnClickPositiveListener(GreeterDialogListener onClickPositiveListener) {
        mOnClickPositiveListener = onClickPositiveListener;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.greeter_dialog, null);

        builder
            .setTitle("Who do you want to greet?")
                // .setMessage("Hello World")
            .setView(view)
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                   EditText etGreetee = (EditText) view.findViewById(R.id.etGreeteeName);
                   mOnClickPositiveListener.onUserApproves(etGreetee.getText().toString());
                }
            })
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                   dismiss();
                }
            });

        return builder.create();
    }

}
