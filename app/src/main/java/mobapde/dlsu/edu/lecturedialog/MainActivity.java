package mobapde.dlsu.edu.lecturedialog;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private Button mBtnCLick;
    private GreeterPresenter mGreeterPresenter;
    private GreeterRepository mGreeterRepository;
    private GreeterDialogListener mGreeterListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtnCLick = (Button) findViewById(R.id.btnClick);
        mGreeterRepository = new GreeterRepository();

        mGreeterPresenter = new GreeterPresenter((TextView) findViewById(R.id.txtContext), mGreeterRepository);
        mGreeterListener = new GreeterDialogListener(mGreeterRepository, mGreeterPresenter);

        mBtnCLick.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GreeterDialog greeterDialog = new GreeterDialog();
                greeterDialog.setOnClickPositiveListener(mGreeterListener);
                greeterDialog.show(getSupportFragmentManager(), "");
            }
        });
    }
}
