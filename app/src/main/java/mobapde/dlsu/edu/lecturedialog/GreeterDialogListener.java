package mobapde.dlsu.edu.lecturedialog;

/**
 * Created by migscabral on 28/09/2017.
 */

public class GreeterDialogListener {
    private GreeterRepository mGreeterRepository;
    private GreeterPresenter mGreeterPresenter;

    public GreeterDialogListener(GreeterRepository greeterRepository, GreeterPresenter greeterPresenter) {
        mGreeterPresenter = greeterPresenter;
        mGreeterRepository = greeterRepository;
    }

    public void onUserApproves(String greetee) {
        mGreeterRepository.setGreetee(greetee);
        mGreeterPresenter.updateView();
    }
}

